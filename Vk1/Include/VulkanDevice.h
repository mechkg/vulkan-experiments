#pragma once

#include "VulkanPhysicalDevice.h"

class VulkanDevice
{
private:
	static VkDevice CreateDevice(const VulkanPhysicalDevice& physicalDevice, const std::set<std::string> &enabledExtensions);
	static VkQueue GetQueueHandle(VkDevice deviceHandle, uint32_t queueFamilyIndex);
public:
	const VkDevice handle;
	const VkQueue commandQueue;

	VulkanDevice(const VulkanPhysicalDevice& physicalDevice, const std::set<std::string>& enabledExtensions);
	~VulkanDevice();
};
