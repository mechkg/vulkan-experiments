#pragma once

#include <plog/Log.h>
#include <vulkan/vulkan_core.h>

#define VK_CHECK_RESULT(f)																				\
{																										\
	VkResult res = (f);																					\
	if (res != VK_SUCCESS)																				\
	{																									\
		PLOGE << "Critical Vulkan error " << res << " in " << __FILE__ << " at line " << __LINE__;      \
		assert(res == VK_SUCCESS);																		\
	}																									\
}

std::string FormatVulkanVersionString(uint32_t version);

std::string FormatQueueTypesString(VkQueueFlags queueFlags);
