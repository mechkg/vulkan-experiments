#pragma once

#include <string>
#include <vector>
#include <set>

std::string JoinStrings(const std::vector<std::string>& strings, const std::string& separator);

template <class T>
bool IsSubset(const std::set<T>& set, const std::set<T>& superset)
{
	for (auto& e : set)
	{
		if (superset.find(e) == superset.end())
			return false;
	}

	return true;
}
