#pragma once

#include <vulkan/vulkan.h>
#include <vector>
#include <optional>
#include <string>
#include <set>

class VulkanPhysicalDevice
{
private:

	static std::vector<VkQueueFamilyProperties> GetQueueFamilyProperties(VkPhysicalDevice deviceHandle);
	static VkPhysicalDeviceProperties GetPhysicalDeviceProperties(VkPhysicalDevice deviceHandle);
	static std::vector<VkExtensionProperties> GetExtensionProperties(VkPhysicalDevice deviceHandle);
	static std::set<std::string> GetExtensionNames(const std::vector<VkExtensionProperties>& extensionProperties);
	static std::optional<uint32_t> GetDefaultGraphicsQueueIndex(const std::vector<VkQueueFamilyProperties>& queueFamilyProperties);
	
public:
	const VkPhysicalDevice handle;
	const VkPhysicalDeviceProperties properties;

	const std::vector<VkQueueFamilyProperties> queueFamilyProperties;
	const std::vector<VkExtensionProperties> extensionProperties;
	const std::set<std::string> supportedExtensionNames;
	const std::optional<uint32_t> defaultGraphicsQueueIndex;

	std::vector<VkSurfaceFormatKHR> GetSurfaceFormats(VkSurfaceKHR surface) const;
	std::vector<VkPresentModeKHR> GetPresentModes(VkSurfaceKHR surface) const;
	
	VulkanPhysicalDevice(VkPhysicalDevice deviceHandle);	
};
