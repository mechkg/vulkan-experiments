#pragma once

#define VK_USE_PLATFORM_WIN32_KHR

#include <vulkan/vulkan.h>
#include <vector>
#include <set>
#include <string>
#include <optional>
#include "VulkanPhysicalDevice.h"
#include <Windows.h>
#include "VulkanDevice.h"

class VulkanInstance
{
private:
	

	static void LogInstanceInfo();
	
	std::optional<VulkanPhysicalDevice> SelectPhysicalDevice(const std::vector<VulkanPhysicalDevice> &devices, const std::set<std::string> &requiredDeviceExtensions) const;
	static VkSurfaceKHR CreateWindowSurface(VkInstance vkInstance, HINSTANCE hinstance, HWND hwnd);
	static VkInstance CreateInstance(const std::set<std::string>& requiredInstanceExtensions);

	bool CheckSurfaceFormatAndPresentModeSupport(const VulkanPhysicalDevice& device) const;
	
public:
	const VkInstance instance;
	const VkSurfaceKHR surface;
	
	VulkanDevice CreateDefaultDevice(const std::set<std::string>& requiredDeviceExtensions = std::set<std::string>()) const;
	
	
	VulkanInstance(HWND hWnd, HINSTANCE hInstance, const std::set<std::string>& requiredInstanceExtensions = std::set<std::string>());
	~VulkanInstance();
};

