#include "VulkanInstance.h"
#include "VulkanUtils.h"

#include <plog/Log.h>
#include "CollectionUtils.h"

void VulkanInstance::LogInstanceInfo()
{
	uint32_t vkInstanceVersion;
	VK_CHECK_RESULT(vkEnumerateInstanceVersion(&vkInstanceVersion));
	PLOGI << "Vulkan instance version: " << FormatVulkanVersionString(vkInstanceVersion);

	uint32_t extensionCount;

	VK_CHECK_RESULT(vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr));

	const auto properties = std::make_unique<VkExtensionProperties[]>(extensionCount);

	VK_CHECK_RESULT(vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, properties.get()));

	PLOGI << extensionCount << " instance extension(s) available:";

	for (uint32_t i = 0; i < extensionCount; i++)
	{
		PLOGI << "\t" << properties[i].extensionName << ", version " << properties[i].specVersion;
	}
}

std::optional<VulkanPhysicalDevice> VulkanInstance::SelectPhysicalDevice(const std::vector<VulkanPhysicalDevice>& devices, const std::set<std::string>& requiredDeviceExtensions) const
{
	// Try finding a suitable discrete GPU first
	for (auto& device : devices)
	{
		if (device.defaultGraphicsQueueIndex.has_value() && IsSubset(requiredDeviceExtensions, device.supportedExtensionNames)
			&& CheckSurfaceFormatAndPresentModeSupport(device)
			&& device.properties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU)
			return std::make_optional(device);
	}

	// If no discrete GPU exists, try any GPU type
	for (auto& device : devices)
	{
		if (device.defaultGraphicsQueueIndex.has_value() && IsSubset(requiredDeviceExtensions, device.supportedExtensionNames)
			&& CheckSurfaceFormatAndPresentModeSupport(device))
			return std::make_optional(device);
	}

	return std::nullopt;
}

VkSurfaceKHR VulkanInstance::CreateWindowSurface(VkInstance vkInstance, HINSTANCE hinstance, HWND hwnd)
{
	VkSurfaceKHR surface;

	VkWin32SurfaceCreateInfoKHR createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
	createInfo.hwnd = hwnd;
	createInfo.hinstance = hinstance;

	VK_CHECK_RESULT(vkCreateWin32SurfaceKHR(vkInstance, &createInfo, nullptr, &surface));

	return surface;
}

VkInstance VulkanInstance::CreateInstance(const std::set<std::string>& requiredInstanceExtensions)
{
	PLOGI << "Initialising Vulkan";

	LogInstanceInfo();

	VkApplicationInfo appInfo = {};
	appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	appInfo.pApplicationName = "Vk1";
	appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.pEngineName = "Mjo";
	appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	appInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	createInfo.pApplicationInfo = &appInfo;

	std::set<std::string> withMustHaveExtensions = {VK_KHR_SURFACE_EXTENSION_NAME, VK_KHR_WIN32_SURFACE_EXTENSION_NAME};

	withMustHaveExtensions.insert(requiredInstanceExtensions.begin(), requiredInstanceExtensions.end());

	std::vector<const char*> extensions;

	for (auto& extName : withMustHaveExtensions)
	{
		extensions.emplace_back(extName.c_str());
	}

	createInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	createInfo.ppEnabledExtensionNames = extensions.data();
	createInfo.enabledLayerCount = 0;

	VkInstance instance;

	VK_CHECK_RESULT(vkCreateInstance(&createInfo, nullptr, &instance));

	return instance;
}

bool VulkanInstance::CheckSurfaceFormatAndPresentModeSupport(const VulkanPhysicalDevice& device) const
{
	auto formats = device.GetSurfaceFormats(surface);

	const bool formatOk = std::find_if(formats.begin(), formats.end(), [](VkSurfaceFormatKHR format)
	{
		return format.format == VK_FORMAT_B8G8R8A8_UNORM && format.colorSpace == VK_COLORSPACE_SRGB_NONLINEAR_KHR;
	}) != formats.end();

	auto presentModes = device.GetPresentModes(surface);

	const bool presentOk = std::find(presentModes.begin(), presentModes.end(), VK_PRESENT_MODE_MAILBOX_KHR) != presentModes.end();

	return formatOk && presentOk;
}


VulkanDevice VulkanInstance::CreateDefaultDevice(const std::set<std::string>& requiredDeviceExtensions) const
{
	uint32_t deviceCount;

	VK_CHECK_RESULT(vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr));

	std::vector<VkPhysicalDevice> devices(deviceCount);

	VK_CHECK_RESULT(vkEnumeratePhysicalDevices(instance, &deviceCount, devices.data()));

	std::vector<VulkanPhysicalDevice> wrappers;

	if (deviceCount > 0) {

		wrappers.reserve(deviceCount);

		for (auto& device : devices)
		{
			wrappers.emplace_back(device);
		}
	}

	std::set<std::string> extensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };
	extensions.insert(requiredDeviceExtensions.begin(), requiredDeviceExtensions.end());

	std::optional<VulkanPhysicalDevice> physicalDevice = SelectPhysicalDevice(wrappers, extensions);

	if (physicalDevice.has_value())
	{
		PLOGI << "Using " << physicalDevice.value().properties.deviceName;

		return VulkanDevice(physicalDevice.value(), extensions);		
	}

	throw std::runtime_error("No suitable Vulkan device present");
}


VulkanInstance::VulkanInstance(HWND hWnd, HINSTANCE hInstance, const std::set<std::string>& requiredInstanceExtensions):
	instance(CreateInstance(requiredInstanceExtensions)),
	surface(CreateWindowSurface(instance, hInstance, hWnd))
{
}

VulkanInstance::~VulkanInstance()
{
	vkDestroySurfaceKHR(instance, surface, nullptr);
	vkDestroyInstance(instance, nullptr);
}
