#include "VulkanUtils.h"
#include "CollectionUtils.h"

#include <vulkan/vulkan.h>

std::string FormatVulkanVersionString(uint32_t version)
{
	std::ostringstream s;
	s << VK_VERSION_MAJOR(version) << "." << VK_VERSION_MINOR(version) << "." << VK_VERSION_PATCH(version);
	return s.str();
}

std::string FormatQueueTypesString(VkQueueFlags queueFlags)
{
	std::vector<std::string> types;

	if (queueFlags & VK_QUEUE_GRAPHICS_BIT)
	{
		types.emplace_back("GRAPHICS");
	}
	if (queueFlags & VK_QUEUE_COMPUTE_BIT)
	{
		types.emplace_back("COMPUTE");
	}
	if (queueFlags & VK_QUEUE_TRANSFER_BIT)
	{
		types.emplace_back("TRANSFER");
	}

	return JoinStrings(types, " | ");
}
