#include "CollectionUtils.h"
#include <sstream>

std::string JoinStrings(const std::vector<std::string>& strings, const std::string& separator)
{
	std::ostringstream ss;
	bool first = true;

	for (auto& s : strings)
	{
		if (!first)
		{
			ss << separator;
		}
		else
		{
			first = false;
		}

		ss << s;
	}

	return ss.str();
}
