#include "VulkanPhysicalDevice.h"
#include "VulkanUtils.h"

std::vector<VkQueueFamilyProperties> VulkanPhysicalDevice::GetQueueFamilyProperties(VkPhysicalDevice deviceHandle)
{
	uint32_t queueFamilyCount;
	vkGetPhysicalDeviceQueueFamilyProperties(deviceHandle, &queueFamilyCount, nullptr);

	std::vector<VkQueueFamilyProperties> queueFamilyProperties;

	if (queueFamilyCount > 0)
	{
		queueFamilyProperties.resize(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(deviceHandle, &queueFamilyCount, queueFamilyProperties.data());
	}

	return queueFamilyProperties;
}

VkPhysicalDeviceProperties VulkanPhysicalDevice::GetPhysicalDeviceProperties(VkPhysicalDevice deviceHandle)
{
	VkPhysicalDeviceProperties properties = {};
	vkGetPhysicalDeviceProperties(deviceHandle, &properties);
	return properties;
}

std::vector<VkExtensionProperties> VulkanPhysicalDevice::GetExtensionProperties(VkPhysicalDevice deviceHandle)
{
	uint32_t extensionCount;
	VK_CHECK_RESULT(vkEnumerateDeviceExtensionProperties(deviceHandle, nullptr, &extensionCount, nullptr));

	std::vector<VkExtensionProperties> extensionProperties;

	if (extensionCount > 0)
	{
		extensionProperties.resize(extensionCount);
		VK_CHECK_RESULT(vkEnumerateDeviceExtensionProperties(deviceHandle, nullptr, &extensionCount, extensionProperties.data()));
	}

	return extensionProperties;
}

std::set<std::string> VulkanPhysicalDevice::GetExtensionNames(const std::vector<VkExtensionProperties>& extensionProperties)
{
	std::set<std::string> names;

	for (auto& props : extensionProperties)
	{
		names.emplace(props.extensionName);
	}

	return names;
}

std::optional<uint32_t> VulkanPhysicalDevice::GetDefaultGraphicsQueueIndex(const std::vector<VkQueueFamilyProperties>& queueFamilyProperties)
{
	uint32_t index = 0;

	for (auto& family : queueFamilyProperties)
	{
		if (family.queueFlags & VK_QUEUE_GRAPHICS_BIT)
			return std::make_optional(index);
		++index;
	}

	return std::nullopt;
}

std::vector<VkSurfaceFormatKHR> VulkanPhysicalDevice::GetSurfaceFormats(VkSurfaceKHR surface) const
{
	uint32_t formatCount;
	vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface, &formatCount, nullptr);

	std::vector<VkSurfaceFormatKHR> formats;

	if (formatCount != 0) {
		formats.resize(formatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(handle, surface, &formatCount, formats.data());
	}

	return formats;
}

std::vector<VkPresentModeKHR> VulkanPhysicalDevice::GetPresentModes(VkSurfaceKHR surface) const
{
	std::vector<VkPresentModeKHR> presentModes;

	uint32_t presentModeCount;
	vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface, &presentModeCount, nullptr);

	if (presentModeCount != 0) {
		presentModes.resize(presentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(handle, surface, &presentModeCount, presentModes.data());
	}

	return presentModes;
}

VulkanPhysicalDevice::VulkanPhysicalDevice(VkPhysicalDevice deviceHandle) : handle(deviceHandle), properties(GetPhysicalDeviceProperties(deviceHandle)),
                                                                            queueFamilyProperties(GetQueueFamilyProperties(deviceHandle)),
                                                                            extensionProperties(GetExtensionProperties(deviceHandle)),
                                                                            supportedExtensionNames(GetExtensionNames(extensionProperties)),
                                                                            defaultGraphicsQueueIndex(GetDefaultGraphicsQueueIndex(queueFamilyProperties))
{
	PLOGI << "\t" << properties.deviceName << " (vendor ID " << properties.vendorID << ")" << ", API version " << FormatVulkanVersionString(properties.apiVersion);

	if (!queueFamilyProperties.empty())
	{
		PLOGI << "\tDevice has " << queueFamilyProperties.size() << " queue families:";

		for (auto& family : queueFamilyProperties)
		{
			PLOGI << "\t\tQueue type: " << FormatQueueTypesString(family.queueFlags) << ", queue count: " << family.queueCount;
		}
	}
	else
	{
		PLOGI << "\tDevice has no available queues";
	}

	if (!extensionProperties.empty())
	{
		PLOGI << "\tDevice supports the following extension(s):";

		for (auto& ext : extensionProperties)
		{
			PLOGI << "\t\t" << ext.extensionName << ", version " << ext.specVersion;
		}
	}
}
