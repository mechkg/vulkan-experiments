#include "VulkanDevice.h"
#include "VulkanUtils.h"

VkDevice VulkanDevice::CreateDevice(const VulkanPhysicalDevice& physicalDevice, const std::set<std::string>& enabledExtensions)
{
	PLOGI << "Creating logical device";
	
	float priority = 1.0f;

	VkDeviceQueueCreateInfo queueCreateInfo = {};
	queueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
	queueCreateInfo.queueFamilyIndex = physicalDevice.defaultGraphicsQueueIndex.value();
	queueCreateInfo.queueCount = 1;
	queueCreateInfo.pQueuePriorities = &priority;

	VkPhysicalDeviceFeatures deviceFeatures = {};

	std::vector<const char*> extensions;
	extensions.reserve(enabledExtensions.size());
	
	for (auto& extName : enabledExtensions)
	{
		extensions.emplace_back(extName.c_str());
	}

	VkDeviceCreateInfo createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	createInfo.pQueueCreateInfos = &queueCreateInfo;
	createInfo.queueCreateInfoCount = 1;
	createInfo.pEnabledFeatures = &deviceFeatures;
	createInfo.enabledExtensionCount = extensions.size();
	createInfo.ppEnabledExtensionNames = extensions.data();
	createInfo.enabledLayerCount = 0;

	VkDevice device;

	VK_CHECK_RESULT(vkCreateDevice(physicalDevice.handle, &createInfo, nullptr, &device));

	return device;
}

VkQueue VulkanDevice::GetQueueHandle(VkDevice deviceHandle, uint32_t queueFamilyIndex)
{
	VkQueue commandQueue;
	vkGetDeviceQueue(deviceHandle, queueFamilyIndex, 0, &commandQueue);
	return commandQueue;
}

VulkanDevice::VulkanDevice(const VulkanPhysicalDevice& physicalDevice, const std::set<std::string>& enabledExtensions) :
	handle(CreateDevice(physicalDevice, enabledExtensions)),
	commandQueue(GetQueueHandle(handle, physicalDevice.defaultGraphicsQueueIndex.value()))
{
}

VulkanDevice::~VulkanDevice()
{
	vkDestroyDevice(handle, nullptr);	
}
